package com.ids.test.mockito.junit.component;

import com.ibm.cloud.objectstorage.AmazonClientException;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3;
import com.ibm.cloud.objectstorage.services.s3.transfer.TransferManager;
import com.ibm.cloud.objectstorage.services.s3.transfer.TransferManagerBuilder;
import com.ibm.cloud.objectstorage.services.s3.transfer.Upload;
import com.ids.test.mockito.junit.exception.model.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class CosConectorComponent {

  @Autowired
  private AmazonS3 amazonS3;

  public Upload uploadFile(String bucketName, String itemName, File file) {
    Upload upload = null;

    if (file.isFile()) {
      TransferManager transferManager = TransferManagerBuilder.standard().withS3Client(amazonS3)
          .withMinimumUploadPartSize(5242880L).withMultipartCopyPartSize(5242880L).build();
      try {
        upload = transferManager.upload(bucketName, itemName, file);
      } catch (AmazonClientException ex) {
        throw new BusinessException("11", "Error to Upload the Log File to COS IBM");
      } catch (Exception ex) {
        throw new BusinessException("19", ex.getMessage());
      }
    }
    return upload;
  }

}
