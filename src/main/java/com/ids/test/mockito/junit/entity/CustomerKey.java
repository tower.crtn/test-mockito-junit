package com.ids.test.mockito.junit.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@Embeddable
public class CustomerKey implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "PERSON_ID")
  private int customerId;

}
