package com.ids.test.mockito.junit.exception.model;

public enum ErrorType {

  ERROR, WARN, INVALID, FATAL

}
