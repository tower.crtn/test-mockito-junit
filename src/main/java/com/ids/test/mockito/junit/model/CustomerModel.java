package com.ids.test.mockito.junit.model;

import lombok.Data;

@Data
public class CustomerModel {

  private int customerId;

  private String name;

  private String lastName;

  private String birthDate;

  private String statusCode;
}
