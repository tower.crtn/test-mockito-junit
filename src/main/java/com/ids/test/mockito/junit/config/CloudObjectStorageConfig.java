package com.ids.test.mockito.junit.config;

import com.ibm.cloud.objectstorage.ClientConfiguration;
import com.ibm.cloud.objectstorage.auth.AWSCredentials;
import com.ibm.cloud.objectstorage.auth.AWSStaticCredentialsProvider;
import com.ibm.cloud.objectstorage.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.ibm.cloud.objectstorage.oauth.BasicIBMOAuthCredentials;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3;
import com.ibm.cloud.objectstorage.services.s3.AmazonS3ClientBuilder;
import com.ids.test.mockito.junit.properties.CloudObjectStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CloudObjectStorageConfig {

  @Autowired
  private CloudObjectStorageProperties properties;

  @Bean
  public AmazonS3 makeClient() {
    AWSCredentials awsCredentials =
        new BasicIBMOAuthCredentials(properties.getApiKey(), properties.getServerInstanceId());

    ClientConfiguration clientConfiguration = new ClientConfiguration();
    clientConfiguration.setUseTcpKeepAlive(true);

    return AmazonS3ClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
        .withEndpointConfiguration(
            new EndpointConfiguration(properties.getEndpointUrl(), properties.getLocation()))
        .withPathStyleAccessEnabled(true).withClientConfiguration(clientConfiguration).build();
  }

}
